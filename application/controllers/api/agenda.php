<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agenda extends CI_Controller
{
	
	
	function __construct()
	{
		
		parent::__construct();
		
		$this->load->model('admin/agendas_model');
		
		$this->load->config('avanbook_config');
		
		$this->load->library('gf');
		
	}
	
	
	function index()
	{
		
		$this->lists();
		
	}
	
	
	function lists()
	{
		
	
		
		$agenda= $this->agendas_model->agendas_list();
		header ( 'Access-Control-Allow-Origin: *' ); 
		
        header('Content-type: application/json');
		echo json_encode($agenda);
		
	}
	
	function lists_limit($ini, $porpage)
	{

		$agenda= $this->agendas_model->agendas_list_limit($ini,$porpage);
		header ( 'Access-Control-Allow-Origin: *' ); 
		
		header('Content-type: application/json');
		echo json_encode($agenda);
		
	}
	
	
}

?>